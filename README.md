Sur ce gitlab, vous pourrez trouver une branche pour chaque TP du cours, avec entre autres les branches :
- TP1, pour les premiers tests unitaire que nous avons pu faire
- TP2, pour l’implémentation des différents DP que nous avons vu en cours, et l’ajout de tests de types différents
- TP3, pour les différents tests que nous avons pu faire sur le scope « Chiffre d’affaires »
- TP4, pour le script powershell du même TP
